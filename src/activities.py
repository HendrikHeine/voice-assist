class Activities:
    def licht():
        print(f"Schalte das Licht an/aus")
            
    def tor():
        print(f"Schalte das Tor an")


    def choose(res):
        if 'Licht'.upper() in res['text'].upper():
            Activities.licht()
            return 0

        elif 'Tor'.upper() in res['text'].upper():
            Activities.tor()
            return 0

        else:
            print("Unknown command")
            return 1
