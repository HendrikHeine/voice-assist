import json
import threading
import time
import gpiozero
import pyttsx3

from activities import Activities

class speech: 
    def __init__(self,startcode,q):
        self.STARTCODE = startcode
        self.q = q

    def thread_timer(self):
        """This will make effects with the LED NeoPixels while waiting for an command"""
        #led = gpiozero.LED(17)
        #self.power_gpio(17,led)
        time.sleep(5)
        #self.close_gpio(17,led)

    def active(self,rec):
        print("active")
        t = threading.Thread(target=self.thread_timer)
        t.start()
        i=0
        while t.is_alive():
            print('Waiting for command...')
            data = self.q.get()
            if rec.AcceptWaveform(data):
                res = json.loads(rec.Result())
                Activities.choose(res)
                t.join()

    def power_gpio(self,GPIO,led):
        print(f"Power {GPIO}")
        try:
            if not led.is_lit:
                led.on()
        except gpiozero.GPIOZeroError as err:
            print("Error occured: {0}".format(err))

    def close_gpio(self,GPIO,led):
        print(f"Close {GPIO}")
        try:
            if led.is_lit:
                led.off()
        except gpiozero.GPIOZeroError as err:
            print("Error occured: {0}".format(err))

class TextToSpeech:
    def __init__(self, volume=1.0) -> None:
        self.engine = pyttsx3.init()
        self.volume = volume
        self.engine.setProperty('volume', volume)

    def test(self):
        self.engine.say("I will speak this text")
        self.engine.runAndWait()

    def changeVolume(self, volume:float):
        self.engine.setProperty('volume', volume)