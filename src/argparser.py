class Args:
    def __init__(self) -> None:
        self.model = 'model'
        self.samplerate = 44100
        self.device = None
        self.blocksize = 8000

class ArgParser:
    validArgs = [
        "--model",
        "--samplerate",
        "--device",
        "--blocksize"
    ]

    def parse(args:list):
        argsObj = Args()
        for arg in args:
            arg:str
            values = arg.split("=")
            if values[0] in ArgParser.validArgs:
                if values[0] == "--model":
                    argsObj.model

                if values[0] == "--samplerate":
                    argsObj.samplerate = int(values[1])

                if values[0] == "--device":
                    argsObj.device = values[1]

                if values[0] == "--blocksize":
                    argsObj.blocksize = int(values[1])

        return argsObj