import os
import queue
import sys
import json
import sounddevice as sd
import vosk

from speech import speech
from argparser import ArgParser
import version



print(f"Py Voice Assist\nVersion: {version.version}\nFrom: {version.date}\n\n")
q = queue.Queue()


def callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    if status:
        print(status, file=sys.stderr)
        pass
    q.put(bytes(indata))


if __name__ == '__main__':
    args = ArgParser.parse(sys.argv)

    print(f"Bloksize: {args.blocksize}")
    print(f"Model: {args.model}")
    print(f"Device: {args.device}")
    print(f"Samplerate: {args.samplerate}\n")

    if not os.path.exists('model'):
        print("Please download a model from https://alphacephei.com/vosk/models and unpack to 'model'")
        exit(1)
    
    model = vosk.Model(args.model)
    speech = speech('computer', q)

    
    with sd.RawInputStream(samplerate=args.samplerate, blocksize=args.blocksize, device=None, dtype='int16', channels=1, callback=callback):
        print('*' * 80)
        rec = vosk.KaldiRecognizer(model, args.samplerate)
        while True:
            data = q.get()
            print("Listening...")
            if rec.AcceptWaveform(data):
                res = json.loads(rec.Result())
                print(res)
                if speech.STARTCODE in res['text']:
                    speech.active(rec)
            else:
                pass