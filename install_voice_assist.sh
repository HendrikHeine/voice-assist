INSTALL_PATH="/usr/local/bin/pyVoice"
SERVICE_PATH="/etc/systemd/system/voiceAssist.service"

if [ ! -d "$INSTALL_PATH" ]; then
    sudo mkdir $INSTALL_PATH
fi

if [ ! -d ".cache" ]; then
    mkdir .cache
    wget -O .cache/vosk-model-small-de.zip https://alphacephei.com/vosk/models/vosk-model-small-de-0.15.zip
fi

unzip -o .cache/vosk-model-small-de.zip -d .cache/
sudo cp -r ./src/* $INSTALL_PATH
sudo cp start.sh $INSTALL_PATH
sudo mv .cache/vosk-model-small-de-0.15 $INSTALL_PATH/model

if [ -f "$SERVICE_PATH" ]; then
    #sudo systemctl stop $SERVICE_PATH
    #sudo systemctl disable $SERVICE_PATH
    sudo rm $SERVICE_PATH
fi

sudo cp voiceAssist.service $SERVICE_PATH
sudo systemctl daemon-reload
#sudo systemctl enable $SERVICE_PATH