# Voice Assist

So since i hate all those big companies, they just want to collect personal data, i decided to make my own voice assistent sooo... what u gonna do google, huh? :D

The foundation of this program belongs to the code from [Stevies Blog](https://steviesblog.de/blog/2021/10/20/offline-sprachassistent-mit-python/). I just use this bc it works and i kinda know what is going on there. So my plan is to make it like Alexa. As soon i've testet some hardware, i can make a full manual for DIY, bc that's what i love.

**Final Platform** <br>
This programm should be executed in final state on a RaspberryPi 4/ZeroW(2) with some more hardware like LEDs, Mic, Speaker and stuff like this

## Getting started
**Install**
```bash
sh install_libs.sh
sh install_voice_assist.sh
```

**Start**
```bash
sh start.sh
```

**Args**
* `--blocksize=[INT]` : ? idk right... is needed to be changed if running on RaspberryPi
* `--device=[STR]` : Select a mic. If empty, it will use the system default
* `--model=[STR]` : path to vosk language model
* `--samplerate=[INT]` : Sample rate from the mic

